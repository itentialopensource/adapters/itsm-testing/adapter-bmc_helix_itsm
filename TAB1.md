# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Bmc_helix_itsm System. The API that was used to build the adapter for Bmc_helix_itsm is usually available in the report directory of this adapter. The adapter utilizes the Bmc_helix_itsm API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The BMC Helix ITSM adapter from Itential is used to integrate the Itential Automation Platform (IAP) with BMC Helix ITSM. With this adapter you have the ability to perform operations such as:

- Create Change Request
- Get Change Request
- Create Incident
- Get Incident

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
