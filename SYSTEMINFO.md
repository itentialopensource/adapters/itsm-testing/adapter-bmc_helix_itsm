# BMC Helix ITSM

Vendor: BMC
Homepage: https://www.bmc.com/

Product: Helix
Product Page: https://www.bmc.com/it-solutions/bmc-helix.html

## Introduction
We classify BMC Helix ITSM into the ITSM (Service Management) domain as BMC Helix ITSM provides ticketing solutions for Incident and Problem Management.

"BMC Helix solutions are resolving enterprise data problems" 
"Automated, AI-driven service management (AISM) capabilities enable faster, more accurate, and more efficient ways of delivering service innovations." 

## Why Integrate
The BMC Helix ITSM adapter from Itential is used to integrate the Itential Automation Platform (IAP) with BMC Helix ITSM. With this adapter you have the ability to perform operations such as:

- Create Change Request
- Get Change Request
- Create Incident
- Get Incident

## Additional Product Documentation
The [API documents for BMC Helix ITSM](https://docs.bmc.com/docs/itsm213/integrating-third-party-applications-with-bmc-helix-itsm-by-using-the-simplified-rest-api-1032989959.html)
The [Authentication for BMC Helix ITSM](https://docs.bmc.com/docs/itsm213/access-and-authentication-for-the-simplified-rest-api-1032989972.html)
