
## 0.3.5 [10-22-2024]

* Add worklog calls

See merge request itentialopensource/adapters/adapter-bmc_helix_itsm!13

---

## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_19:48PM

See merge request itentialopensource/adapters/adapter-bmc_helix_itsm!15

---

## 0.3.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-bmc_helix_itsm!12

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_17:56PM

See merge request itentialopensource/adapters/adapter-bmc_helix_itsm!11

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_19:09PM

See merge request itentialopensource/adapters/adapter-bmc_helix_itsm!10

---

## 0.3.0 [07-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-bmc_helix_itsm!9

---

## 0.2.4 [03-27-2024]

* Changes made at 2024.03.27_13:57PM

See merge request itentialopensource/adapters/itsm-testing/adapter-bmc_helix_itsm!8

---

## 0.2.3 [03-13-2024]

* Changes made at 2024.03.13_15:01PM

See merge request itentialopensource/adapters/itsm-testing/adapter-bmc_helix_itsm!7

---

## 0.2.2 [03-11-2024]

* Changes made at 2024.03.11_14:42PM

See merge request itentialopensource/adapters/itsm-testing/adapter-bmc_helix_itsm!6

---

## 0.2.1 [02-28-2024]

* Changes made at 2024.02.28_11:05AM

See merge request itentialopensource/adapters/itsm-testing/adapter-bmc_helix_itsm!5

---

## 0.2.0 [12-30-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-bmc_helix_itsm!4

---

## 0.1.3 [06-20-2023]

* Merge poc branch to master

See merge request itentialopensource/adapters/itsm-testing/adapter-bmc_helix_itsm!3

---

## 0.1.2 [01-31-2023]

* Merge poc branch to master

See merge request itentialopensource/adapters/itsm-testing/adapter-bmc_helix_itsm!3

---

## 0.1.1 [10-31-2022]

* Bug fixes and performance improvements

See commit 79f195f

---
